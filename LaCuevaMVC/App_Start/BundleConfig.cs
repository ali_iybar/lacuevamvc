﻿using System.Web;
using System.Web.Optimization;

namespace LaCuevaMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/css").Include(

                ));
            bundles.Add(new ScriptBundle("~/bundles/js").Include(

                        ));
            
            
        }
    }
}
