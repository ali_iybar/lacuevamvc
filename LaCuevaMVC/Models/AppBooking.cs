﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LaCuevaMVC.Models
{
    public class AppBooking
    {
        [Key]
        public int Id { get; set; }

        public string BookDate { get; set; }
        public string BookTime { get; set; }
        public string BookName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateTime { get; set; }
        public string NumberOfPerson { get; set; }
        public string ReplyTo { get; set; }
        public string FormType { get; set; }
        public string FileName { get; set; }
    }
}