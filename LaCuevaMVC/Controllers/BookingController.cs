﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using LaCuevaMVC.DAL;
using LaCuevaMVC.Models;

namespace LaCuevaMVC.Controllers
{
    public class BookingController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Page = "Booking";
            TempData["message"] = "";
            AppBooking model = new AppBooking();
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendForm(AppBooking model)
        {
            if (ModelState.IsValid)
            {

                model.CreateDate = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
                model.CreateTime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
                model.FormType = "Booking";
                if (model.Email.ToString().Length > 1 && model.Email.Contains("@"))
                {
                    model.ReplyTo = model.Email;
                }
                else
                {
                    model.ReplyTo = WebConfigurationManager.AppSettings["BookingMailCc"];
                }
            }
//--------------------------
            Email mail = new Email();
            mail.Send(model);
            TempData["message"] = "Booking Form Submitted Successfuly";
            return View("Done");
        }
    }
}