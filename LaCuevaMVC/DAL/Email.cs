﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;
using System.Xml.XPath;
using LaCuevaMVC.Models;

namespace LaCuevaMVC.DAL
{
    public class Email
    {
        private MailMessage _mail;
        private SmtpClient _smtpServer;

        public Email()
        {
            try
            {
                _smtpServer = new SmtpClient(WebConfigurationManager.AppSettings["SMTP"])
                {

                    Credentials =
                        new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["SMTPusername"],
                            WebConfigurationManager.AppSettings["SMTPpassword"]),
                    EnableSsl = true,
                    Port = Convert.ToInt32(WebConfigurationManager.AppSettings["SMTPport"])
                };
                _mail = new MailMessage()
                {
                    From = new MailAddress(WebConfigurationManager.AppSettings["SMTPFromAddress"]),
                };
            }
            catch (Exception ex)
            {
                throw new Exception("Error in constructing SMTP server ",
                    new Exception(
                        "possibly one of the smtp username, password, port or from address parameters is null or empty"));
            }
        }


        public void Send(AppBooking Booking)
        {
            try
            {
                _mail.ReplyToList.Add(Booking.ReplyTo);
            }
            catch (Exception e)
            {
                _mail.ReplyToList.Add(WebConfigurationManager.AppSettings["BookingMailTo"]);
            }
            try
            {
                _mail.To.Add(WebConfigurationManager.AppSettings["BookingMailTo"]);
                _mail.To.Add(WebConfigurationManager.AppSettings["BookingMailCc"]);

              

                if (Booking.FormType == "Booking")
                {
                    _mail.Subject = "Booking - " + Booking.BookName + " (" + Booking.NumberOfPerson + ") " +
                                    Booking.BookDate + " - " + Booking.BookTime;
                }
                if (Booking.FormType == "Contact")
                {
                    _mail.Subject = "Contact - " + Booking.BookName;
                }
                Booking.FileName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +
                                   DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() +
                                   DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() +
                                   DateTime.Now.Millisecond.ToString();
                _mail.Body = PrepareBody(Booking); // PrepareBodyWithSkin(mContact,1) ;
                _mail.IsBodyHtml = true;

            }
            catch (Exception ex)
            {
                SaveError(Booking, "Either To, Cc, Bcc, Subject or Body fields has problem");
                throw new Exception("Error creating Mail object",
                    new Exception("Either To, Cc, Bcc, Subject or Body fields has problem"));

            }
            try
            {
                _smtpServer.Send(_mail);
            }
            catch (Exception ex)
            {
                SaveError(Booking, "Error in sending mail via SMTP. Possibly SMTP server problem.");
                throw new Exception("Error sending email",
                    new Exception("Error in sending mail via SMTP. Possibly SMTP server problem."));
            }
            try
            {
                SaveEmail(Booking);
            }
            catch (Exception ex)
            {

                throw new Exception("Error saving email",
                    new Exception("SaveEmail created this error after sending email"));
            }
        }


        public void SaveEmail(AppBooking Booking)
        {
            var FilePath = HttpContext.Current.Server.MapPath("~/App_Data/");
            if (System.IO.Directory.Exists(FilePath))
            {
                try
                {
                    System.IO.File.WriteAllText(FilePath + Booking.FileName, _mail.Body);
                }
                catch (Exception)
                {

                    throw new Exception("Error in save file process");
                }
            }
        }

        public void SaveError(AppBooking booking, string error)
        {
            var FilePath = HttpContext.Current.Server.MapPath("~/App_Data/");
            if (System.IO.Directory.Exists(FilePath))
            {
                try
                {
                    System.IO.File.WriteAllText(FilePath + "__" + booking.FileName, error + "<br/><hr/><br/> " +_mail.Body);
                }
                catch (Exception)
                {

                    throw new Exception("Error in save file process");
                }
            }
        }

        public string PrepareBody(AppBooking model)
        {
            string mBody;
            mBody = string.Format("<table style='width:800px'>" +
                                  "<tr><td style='background-color:#CCCCCC;width:200px;'>Send Date</td><td>{0}</td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Send Time</td><td>{1}</td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Name</td><td>{2}</td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Email</td><td><a href='mailto:{3}'>{3}</a></td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Nr Of Person</td><td>{8}</td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Phone</td><td><a href='tel:{4}'>{4}</a></td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Booking Date</td><td>{5}</td></tr>" +
                                  "<tr><td style='background-color:#CCCCCC;'>Booking Time</td><td>{6}</td></tr>" +
                                  "<tr><td colspan='2' style='background-color:#CCCCCC;'>Comment</td></tr>" +
                                  "<tr><td  colspan='2'>{7}</td></tr>" +
                                  "</table>", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(),
                model.BookName, model.Email, model.Phone, model.BookDate, model.BookTime, model.Comment,
                model.NumberOfPerson);
            return mBody;
        }
    }

}